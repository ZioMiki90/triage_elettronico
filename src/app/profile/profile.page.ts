import { Component, OnInit } from '@angular/core';
import {WsserviceService} from '../wsservice.service';
import {NavController} from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
    user: any = {};

  constructor(public WsService: WsserviceService, public navCtrl: NavController) {

  }
    ngOnInit() {
        // this.WsService.showLoading().then();
        const self = this;
        this.WsService.WsLoginStatus().then(
          (res: any) => {
              if (res.status === 0) {
                  this.navCtrl.navigateRoot('login').then();
                  // setTimeout( () => {
                  //   self.WsService.loader.dismiss().then();
                  // }, 500);
                  // this.WsService.hideLoading().then();
              } else {
                  if (res.status === 401) {
                      this.WsService.doAlertunathorize('Errore', 'Non sei autorizzato. Effettua il login').then();
                      // setTimeout( () => {
                      //     self.WsService.loader.dismiss().then();
                      // }, 500);

                  } else {
                      this.user = res;
                      // setTimeout( () => {
                      //     self.WsService.loader.dismiss().then();
                      // }, 500);
                  }
              }
          }
      );
  }

  logout() {
    this.navCtrl.navigateRoot('logout').then();
  }
}
