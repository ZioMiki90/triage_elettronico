import { Component, OnInit } from '@angular/core';
import {WsserviceService} from '../wsservice.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import {NavController, Platform} from '@ionic/angular';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  historyTriage: any;
  history: any;
  platformSystem: any;
  support: any;

  constructor(public WsService: WsserviceService, private transfer: FileTransfer, public file: File, public platform: Platform, public navCtrl: NavController) {
      if (this.platform.is('ios') || this.platform.is('android')) {
          this.platformSystem = 'mobile';
          console.log(this.platform);
      } else {
          this.platformSystem = 'desktop';
      }
      // let element = document.getElementById(String(0) );
      // element.classList.remove('selected');
      // element = document.getElementById(String(1) );
      // element.classList.add('selected');
  }

  doRefresh(event) {
      this.getTriage();
      event.target.complete();

  }
  ngOnInit() {
    this.getTriage();
  }
  getTriage(time = 500) {
      const self = this;
      // this.WsService.showLoading().then();
      this.WsService.getTriages().then(
          (res: any) => {
              if (res.status === 0)  {
                  this.navCtrl.navigateRoot('login').then();
                  // setTimeout( () => {
                  //     self.WsService.loader.dismiss().then();
                  // }, time);
                  } else {
                  if (res.status === 401) {
                      this.WsService.doAlertunathorize('Errore', 'Non sei autorizzato. Effettua il login').then();
                      // setTimeout( () => {
                      //     self.WsService.loader.dismiss().then();
                      // }, time);
                  } else {
                      console.log(res);
                      this.historyTriage = res;
                      this.support = res;
                      this.historyTriage = this.historyTriage.reverse();
                      // setTimeout( () => {
                      //     self.WsService.loader.dismiss().then();
                      // }, time);
                  }
              }

          }
      );
  }
    download(triageId, nome, cognome) {
      const fileTransfer: FileTransferObject = this.transfer.create();
      this.file.checkDir(this.file.externalRootDirectory, 'Download_triage').then(
            (response) => {
                // tslint:disable-next-line:max-line-length
                fileTransfer.download('http://64.227.74.93:8000/anamnesi/get_pdf/?id_triage=' + triageId, this.file.externalRootDirectory + '/Download_triage/' + nome + '_' + cognome + '.pdf').then((entry) => {
                    this.WsService.doAlert('File scaricato', 'Il file è stato scaricato correttamente in ' + entry.toURL());
                    console.log('download complete: ' + entry.toURL());
                }, (error) => {
                    this.WsService.doAlert('File scaricato', 'Il file già scaricato');
                });
            }
        ).
        catch(err => {

          this.file.createDir(this.file.externalRootDirectory, 'Download_triage', false).then(response => {
              console.log('Directory created', response);
              // tslint:disable-next-line:max-line-length
              fileTransfer.download('http://64.227.74.93:8000/anamnesi/get_pdf/?id_triage=' + triageId, this.file.externalRootDirectory + '/Download_triage/' + nome + '_' + cognome + '.pdf').then((entry) => {
                  this.WsService.doAlert('File scaricato', 'Il file è stato scaricato correttamente in ' + entry.toURL());
                  console.log('download complete: ' + entry.toURL());
              }, (error) => {
                  this.WsService.doAlert('File scaricato', 'Il file già scaricato');
              });
          });
      });
    }

    getNameSurname(ev: any) {
        this.historyTriage = this.support;
        let temporary: any = [];
        let query2: any;

        const query = ev.target.value.toLowerCase();
        query2 = query;

        if (query2.length > 0) {
            const first = query2.charAt(0).toUpperCase();
            if (query2.length >= 2) {
                query2 = first + query2.substr(1, query2.length);
            } else {
                query2 = first;
            }
        }

        this.historyTriage.forEach((values: any) => {
            // console.log(values.cliente)
            if (values.cliente.nome.includes(query) || values.cliente.cognome.includes(query) || values.cliente.nome.includes(query2) ||  values.cliente.cognome.includes(query2)) {
                console.log(values)
                temporary.push(values);
            }
        });

        this.historyTriage = temporary;
        console.log(this.historyTriage);
    }

    deleteTriage(idTriage) {
      const temporary = [];
      this.WsService.deleteTriages(idTriage).then(
          (res: any) => {
              if (res.status === 204) {
                  this.historyTriage.forEach((values: any) => {
                      // console.log(values.cliente)
                      if (values.id !== idTriage) {
                          console.log(values)
                          temporary.push(values);
                      }
                  });
                  this.historyTriage = temporary;
              } else {
                  this.WsService.doAlert('Errore' , 'Errore cancellazione Triage. Contattare l\'amministratore.');
              }
          }
      );

    }

}
