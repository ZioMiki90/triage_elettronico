import { Component, OnInit } from '@angular/core';
import {AlertController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(public alertController: AlertController, public navCtrl: NavController) { }

  ngOnInit() {
    this.presentAlertConfirm().then();
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Logout!',
      message: 'Vuoi davvero effettuare il logout?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.navCtrl.navigateRoot('profile');
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Si',
          handler: () => {
            window.localStorage.setItem('Token', '');
            this.navCtrl.navigateRoot('login');
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }


}
