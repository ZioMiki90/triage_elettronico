import { TestBed } from '@angular/core/testing';

import { WsserviceService } from './wsservice.service';

describe('WsserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WsserviceService = TestBed.get(WsserviceService);
    expect(service).toBeTruthy();
  });
});
