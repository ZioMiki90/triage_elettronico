import { Component, OnInit } from '@angular/core';
import {NavController} from '@ionic/angular';
import {NgForm} from '@angular/forms';
import {WsserviceService} from '../wsservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginLabel = 'Accedi';
  errorMessage: any;
  height: any;
  heightDiv: any;
  constructor(public navCtrl: NavController, public WsService: WsserviceService) {
      this.height = window.innerHeight;
      this.heightDiv = this.height / 2;
      // console.log(this.heightDiv);
  }

  ngOnInit() {
      const self = this;
      // this.WsService.showLoading().then();
      this.WsService.WsLoginStatus().then(
        (res: any) => {
            console.log(res);
            if (res.status === 0) {
                console.log('Non c\'è connessione');
                setTimeout( () => {
                    // self.WsService.loader.dismiss().then();
                }, 3000);
                this.WsService.doAlert('Errore', 'Problema con il server. Controlla la connessione.').then();
            } else {
                if (res.status === 401) {
                    // self.WsService.loader.dismiss().then();
                    console.log('Non sei Autorizzato');
                } else {
                    // self.WsService.loader.dismiss().then();
                    this.navCtrl.navigateRoot('history').then();
                }
            }
        }
    );
  }
  login(LoginForm: NgForm) {
      const self = this;
    this.WsService.showLoading().then();
    this.errorMessage = '';
    const user =  LoginForm.value.username;
    const pass =  LoginForm.value.password;
    this.WsService.login(user, pass)
        .then(
            (data: any) => {
                if (data.status === 401) {
                    this.WsService.doAlert('Errore', 'Username o password errati.');
                    this.loginLabel = 'Riprova';
                    setTimeout( () => {
                        self.WsService.loader.dismiss().then();
                    }, 500);

                } else {
                    setTimeout( () => {
                        self.WsService.loader.dismiss().then();
                    }, 500);
                    this.loginLabel = 'Disconnetti';
                    this.navCtrl.navigateRoot('history').then();
                }

              // in caso di successo vado alla pagine dei triage
              // this.WsService.hideLoading().then();
            },
            error => {
                if (error.status === 0) {
                    this.WsService.doAlert('Errore', 'Problema di comunicazione col server. Controlla connessione.');
                }

                    this.loginLabel = 'Riprova';
                    this.errorMessage = error.message;
                setTimeout( () => {
                    self.WsService.loader.dismiss().then();
                }, 1500);
                    // this.WsService.hideLoading().then();
            }
        );
  }
}
