import { Injectable } from '@angular/core';
import {AlertController, LoadingController, NavController, ToastController} from '@ionic/angular';
import {Headers, Http, RequestOptions} from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class WsserviceService {
  // tslint:disable-next-line:max-line-length
  constructor(public alertCtrl: AlertController, public http: Http, public loadingCtrl: LoadingController, private toastController: ToastController, public navCtrl: NavController) {
    this.label = this.labelIta;
  }
  bodyContentHeigth: any = false;
  showCookie: any;
  userInfo: any;
  labelIta = {

  };
  loginLabel = 'Accedi';
  loader = null;
  label: any;
  domain: any = 'http://64.227.74.93:8000';
  headerHeight: any = false;

  getHeight() {

    if (!this.headerHeight) {
      this.headerHeight = document.getElementById('ionTabs').clientHeight;
    }
    return this.headerHeight;
  }

  async  doAlert(title: string, msg: string, dismiss= true) {
    const alert = await this.alertCtrl.create({
      header : title,
      message: msg,
      cssClass: 'alert-servizi',
      buttons: [
        {
          text: 'Ok',

          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }
  async  doAlertunathorize(title: string, msg: string, dismiss= true) {
    const alert = await this.alertCtrl.create({
      header : title,
      message: msg,
      cssClass: 'alert-servizi',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Vai al login',

          handler: () => {
            this.navCtrl.navigateRoot('login').then();
          }
        }
      ]
    });
    await alert.present();
  }


  getHttpHeader(noatuh = false, type = 'none') {
    let tokenAttuale = window.localStorage.getItem('Token');
    console.log("Token attuale "+tokenAttuale);
    if (!tokenAttuale) {
      tokenAttuale = '';
    }
    const headers = new Headers();

    if (type !== 'none') {

    } else {
      headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    }
    headers.append('Cache-Control', 'no-cache');
    if (!noatuh) {
      headers.append('Authorization', 'Token ' + tokenAttuale);
    }
    return new RequestOptions({ headers });
  }

  login(user: string, pass: string ) {
    // this.showLoading().then();
    console.log('login ' + this.domain);
    const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    const options = new RequestOptions({ headers });

    return new Promise((resolve, reject) => {
      console.log('WS Login Success: ' + user);
      console.log('WS Login Success: ' + pass);
      console.log('WS Login Success: ' + JSON.stringify(options));
      this.http.post( this.domain + '/anamnesi/login/',
          'username=' + user + '&password=' + pass,
          options)
          .subscribe(data => {
                const response = data.json();
                console.log(response);
                this.loginLabel = 'Disconnetti';
                window.localStorage.setItem('Token', response.token);
                resolve(response);
                // this.hideLoading().then();

              },
              error => {
                console.log('WS Login Error: ' + JSON.stringify(error));
                const response = error.json();
                let dead = true;

                console.log('WS Login Error: ' + JSON.stringify(response));
                this.loginLabel = 'Riprova';
                // this.user_haspermission = false;
                // this.user_isrivenditore = false;
                let msg = response;
                if (error.status === '0' || error.status === 0) {
                  msg = 'Server non trovato.';
                }
                if (error.status === '401' || error.status === 401 || error.status === '406' || error.status == 406) {
                  msg = 'Username o password errati.';
                  dead = false;
                }
                if (response.name === 'TimeoutError') {
                  msg = 'Timeout connessione';
                }
                resolve(response);
                window.localStorage.setItem('Token', ''); // !IMPORTANT cancello qualunque WStoken esistente su LocalStorage
                // this.hideLoading().then();

              }
          );
    });

  }

  WsLoginStatus() {
    // this.showLoading().then();
    console.log('WsLoginStatus ' + this.domain);
    console.log('Try WsLoginStatus with loaded WSkey token: ' + window.localStorage.getItem('Token'));
    const options = this.getHttpHeader();
    const key = window.localStorage.getItem('Token');

    return new Promise((resolve, reject) => {

      // Fix by ZioMiki
      if ( key == null) {
        const res =  {
          label: 'Accedi',
        };
        reject(res);
      }

      this.http.post( this.domain + '/anamnesi/check_authentication/',
          JSON.stringify({Token: key }),
          options)
          .subscribe(data => {
                const response = data.json();
                console.log('WSLoginStatus Success: ' + JSON.stringify(response));
                // if (!response.res) { // il token non è valido.
                //   console.log('REJECTED');
                //   reject(response);
                // } else {
                resolve(response);
                // this.hideLoading().then();
                // }
              },
              error => {
                const response = error; // .json();
                let dead = true;
                // console.log("WSLoginStatus Error: " + JSON.stringify(error));
                console.log('WSLoginStatus ERRORa: ' + JSON.stringify(response));
                this.loginLabel = 'Accedi';
                let msg = response;
                if (response.status === '0' || response.status === 0) {
                  msg = 'Server non trovato.';
                }
                if (response.status === '401' || response.status === 401) {
                  msg = 'Username o password errati.';
                  dead = false;
                }
                if (response.status === '500' || response.status === 500) {
                  msg = 'Effettua il login.';
                  dead = false;
                }
                if (response.name === 'TimeoutError') {
                  msg = 'Timeout connessione';
                }
                resolve(response);
                // this.hideLoading().then();

              }
          );
    });
  }


  async showLoading(time = 15000) {
    console.log("Show loading");
    this.loader = await this.loadingCtrl.create({
      message: 'Attendere...',
      duration: time
    });
    await this.loader.present();
  }
  async hideLoading() {
    console.log("Hide loading");

    if (this.loader) {
      await this.loader.dismiss();
    }
  }

  async ToastCookie() {
    console.log('ciao 2');
    const toast =  await this.toastController.create({
      header: 'Cookie',
      message: this.label.cookie,
      position: 'bottom',
      color: 'dark',
      buttons: [
        {
          side: 'start',
          icon: 'star',
          text: 'Ok',
          handler: () => {
            window.localStorage.setItem('cookie', 'yes');
          }
        }
      ]
    });
    toast.present();
  }

  getTriages() {
    return new Promise((resolve, reject) => {
      const options = this.getHttpHeader();
      this.http.get(     this.domain + '/anamnesi/get_triage/',
          options
      ).subscribe(data => {
            const response = data.json();
            console.log(response);
            resolve(response);
          },
          error => {
            // console.log(error);
            resolve(error)
            console.log('Errore get triages, controllare la connessione e riprovare!');
          }
      );
    });

  }

  deleteTriages(idTriage) {
    return new Promise((resolve, reject) => {
      const options = this.getHttpHeader();
      this.http.delete(     this.domain + '/anamnesi/triage/' + idTriage + '/delete/',
          options
      ).subscribe(data => {
            const response = data.json();
            console.log(data);
            resolve(data);
          },
          error => {
            // console.log(error);
            resolve(error)
            console.log('Errore delete triages, controllare la connessione e riprovare!');
          }
      );
    });

  }

}
